const fetch = require('node-fetch');
const functions = require('firebase-functions');
const FormData = require('form-data');
exports.liquipedia = functions.https.onRequest(async (req, res) => {
    const fd = new FormData();
    fd.append('apikey', functions.config().liquipedia.apikey);
    Object.entries(JSON.parse(req.body)).forEach(([key, value]) => fd.append(key, value));
    console.log("FORM DATA: " + JSON.stringify(fd, null, 2));
    const json = await fetch(`https://api.liquipedia.net${req.path}`, {
        method: 'POST',
        body: fd,
    }).then(res => res.json());
    res.set('Access-Control-Allow-Origin', '*');
    res.set('Access-Control-Allow-Methods', 'POST, OPTIONS');
    res.send(json);
});
