import { Api } from './api';
import { AppStore } from './App.store';

export const MatchHistoryService = {
    loadMatchHistory(playerId) {
        Api.getMatchHistory(playerId)
            .then(matches => {
                // AppStore.set({
                //     events: transfers.map(t => ({
                //         when: new Date(t.date).getTime(),
                //         description: `From ${t.fromteam} to ${t.toteam}`
                //     }))
                // })
                console.log(matches)
                const wins = matches.map(match => {
                    if (Object.entries(match.opponent1players).map(([key, value]) => value).includes(playerId)) {
                        console.log("corejj was in opponent1");
                        return {
                            when: new Date(match.date).getTime(),
                            wins: match.opponent1score,
                            loss: match.opponent2score,
                        };
                    } else {
                        console.log("assuming corejj was in opponent2");
                        return {
                            when: new Date(match.date).getTime(),
                            wins: match.opponent2score,
                            loss: match.opponent1score,
                        };
                    }
                });
                AppStore.set({ wins });
            });
    }
}