
const apiUrl = 'https://us-central1-liquipedia-pog.cloudfunctions.net/liquipedia';
export const Api = {
    getPlayerTransfers(playerId) {
        return apiFetch('/api/v1/transfer', {
            wiki: 'leagueoflegends',
            conditions: `[[player::${playerId}]]`,
        });
    },
    getAllPlayers() {
        return apiFetch('/api/v1/player', {
            wiki: 'leagueoflegends',
            limit: 50,
        });
    },
    getDatapoints(pageid) {
        return apiFetch('/api/v1/datapoint', {
            conditions: `[[pageid::${pageid}]]`
        });
    },
    getMatchHistory(playerId) {
        return apiFetch('/api/v1/matchfeed', {
            wiki: 'leagueoflegends',
            type: 'player',
            name: playerId,
        });
    }
}

function apiFetch(route, body) {
    return fetch(`${apiUrl}${route}`, {
        method: 'POST', body: JSON.stringify(body)
    }).then(res => {
        if (res.status > 299) {
            throw new Error('Api sad :(')
        }

        return res.json();
    }).then((json) => json.result);
}