import Timeline from './Timeline';
import AllPlayers from './Players/AllPlayers';

export default [
    { path: '/', component: AllPlayers },
    { path: '/timeline/:playerId', component: Timeline },
];
