const { Store } = require('svelte/store');
import { MatchHistoryService } from './match-history.service';
import { TimelineService } from './timeline.service';

export const AppStore = window.store = new (class _AppStore extends Store {
    constructor() {
        super();
        this.set({
            events: [
                { when: Date.now(), icon: 'poggers.png', description: 'On this day, things happened' },
                { when: Date.now() - (1000 * 60 * 60 * 24 * 7), icon: 'poggers.png', description: 'On this day, things happened' }
            ],
            players: [],
        });

        this.on('state', ({ changed, current }) => {
            if (changed.playerId) {
                TimelineService.loadPlayerEvents(current.playerId)
                MatchHistoryService.loadMatchHistory(current.playerId);
            }
        });
    }
})();
