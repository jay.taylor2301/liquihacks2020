import { Api } from './api';
import { AppStore } from './App.store';

export const TimelineService = {
    loadPlayerEvents(playerId) {
        Api.getPlayerTransfers(playerId)
            .then(transfers => {
                AppStore.set({
                    events: transfers.map(t => ({
                        when: new Date(t.date).getTime(),
                        description: `From ${t.fromteam} to ${t.toteam}`
                    }))
                })
            })
    }
}